@ MC404 - LAB08
@ VITOR ALVES ARRAIS DE SOUZA
@ 120263

	.globl main
	.org   0x1000

main:

    ldr r0, vetor
    mov r1, 20
    mov r2, 12

@ encontra elemento
@ entradas: r0 contém o endereço do vetor, r1 o número de elementos e em r2 o valor procurado
@ saida: r0 elemento se encontrou, -1 se nao encontrou
busca:
    mov r3, r0		@ r3 -> esquerda do vetor
    mul r4, r1, 4	@ nº de elementos x 4bytes
    add r4, r4, r0		
    sub r4, r4, 4	@ r4 -> direita do vetor
loop:
    add r5, r3, r4	@ 
    lsr r5		@ m = (e + d)/2	
    ldr r6, (r5)	@ r6 = vetor[m]
    cmp r6, r2		@ compara r6 com o elemento buscado
    beq fim_sucesso	@ encontrou x! busca finalizada com sucesso
    cmp r6, r2		@ nao encontrou! é maior ou menor que x?
    bgt maior		@ maior! caso contrario, eh menor
    mov r4, r5		@ d = m-1
    sub r4,r4, 4		@ 
    cmp r3, r4		@ e >= r4?
    bge fim_fracasso	@ se sim, busca eh finalizada sem sucesso
    b loop
maior:
    mov r3, r5		@ 
    add r3, r3, 4	@ e = m+1
    cmp r3, r4		@ e>= r4 ?
    bge fim_fracasso	@ se sim, busca eh finalizada sem sucesso
    b loop
fim_sucesso:
    mov r0, r2
    b fim
fim_fracasso:
    mov r0, -1
fim:

vetor:
    .word	1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20