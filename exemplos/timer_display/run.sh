#!/bin/sh
if [ -z "$1" ]
then
    echo "No argument supplied"
else
    if [ -z "$2" ]
    then
        echo "Supply the OS to build"
    else
        ../../arm/bin/arm-none-eabi-$2-as $1.s -o $1
        ../../arm/bin/jarm -c -l $1 -d devices.txt -lc input.txt
    fi
fi
