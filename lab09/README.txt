*****************
** OBSERVACOES **
*****************
O jogo possui 30 sequencias de 20 cores e todas as sequencias sao diferentes duas a duas.
Essa escolha implica em: 
1) O jogo é finito, ou seja, se a pessoa acertar a sequencia de 20 cores, o jogo acaba.
2) O fato de as sequencias serem fixas (30 sequencias) permite a fraudulacao do jogo, pois a pessoa pode gravar todas as sequencias.
3) O jogo é ciclico, ou seja, ao apertar start a 1º sequencia eh escolhida. Ao apertar start novamente, a 2º sequencia eh escolhida. Depois da 30º sequencia, volta novamente para 1º. 
