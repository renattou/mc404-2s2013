@ modos de interrupção no registrador de status
	.set IRQ_MODE,	0x12
	.set FIQ_MODE, 	0X11
	.set USER_MODE,	0x10

@ flag_timer para habilitar interrupções externas no registrador de status
	.set IRQ, 0x80
	.set FIQ, 0x40

@enderecos dispositivos
	.set LEDS,	0x90110
	.set TIMER,  	0x90008
	.set B_RED,	0x90010
	.set B_BLUE,	0x90011
	.set B_GREEN,	0x90012
	.set B_YELLOW,	0x90013
	.set B_START,   0x90014

@ constantes
	.set INTERVAL, 150
	.set RED,       0
	.set BLUE,      1
	.set GREEN, 	2
	.set YELLOW, 	3
	.set START,     10
	.set B_READY,   1

@ vetor de interrupções
	.org  6*4               @ preenche apenas uma posição do vetor,
	                        @ correspondente ao tipo 6
	b       tratador_timer

	.org  7*4

	b 	tratador_botao

main:
	mov	sp,#0x400		@ seta pilha do modo supervisor
	mov	r0,#IRQ_MODE		@ coloca processador no modo IRQ (interrupção externa)
	msr	cpsr,r0			@ processador agora no modo IRQ
	mov	sp,#0x300		@ seta pilha de interrupção IRQ
	mov	r0,#USER_MODE		@ coloca processador no modo usuário
	bic 	r0,r0,#IRQ      	@ interrupções IRQ habilitadas
	mov 	r0, #FIQ_MODE		@ coloca processador no modo FIQ
	msr 	cpsr, r0		@ processador agora no modo FIQ
	mov 	sp, #0x500		@ seta pilha de interrupcao FIQ
	mov 	r0, #USER_MODE		@ coloca processador no modo USER
	bic 	r0, r0, #FIQ  		@ interrupões FIQ habilitadas
	msr 	cpsr, r0		@ processador agora no modo USER
	mov	sp,#0x10000		@ pilha do usuário no final da memória 

	ldr	r0,=INTERVAL
	ldr	r6,=TIMER
	str 	r0,[r6]			@ seta timer


	mov 	r5, #-1			@ numero de start pressionados
start:
	ldr 	r1, =flag_botao
	ldr 	r0, [r1]
	cmp 	r0, #10
	bne 	start       		@ espera por botao start
start_n:
	mov 	r8, #0			@ nivel do jogador
	mov 	r0, #-1     		@ reseta flag_botao
	str 	r0, [r1]

	ldr 	r0, =seq_atual		@ por padrao o jogo comeca com
	ldr 	r1, =seq1		@ a seq1

	mov 	r5, r5, lsl #3
	add	r1, r1, r5		@ avanca para proxima sequencia
	mov	r5, r5, lsr #3
	str	r1, [r0]

pre_acende_led:
	mov 	r4, #0
	mov 	r0, #0
	ldr 	r1, =flag_timer
	str 	r0 ,[r1]		@ zera flag_timer
	
	mov 	r2, #8			@ numero de intervalos de tempo a esperar
	sub	r2, r2, r8		@ baseado no nivel

acende_led:
	ldr	r1,=flag_timer
	ldr	r0,[r1]
	cmp	r0,#0			@ espera interrupcao do timer
	beq	acende_led
	mov	r0,#0			@ reseta flag_timer
	str	r0,[r1]
	sub	r2, r2, #1
	cmp	r2, #0
	bne	acende_led		@ avanca se tiver passado os N intervalos
	
	ldr 	r0, =leds
	ldr 	r1, =seq_atual
	ldr 	r1, [r1]
	ldrb 	r1, [r1,r4]
	ldrb 	r0, [r0,r1]
	ldr 	r6, =LEDS
	strb 	r0, [r6]		@ acende proximo led da sequencia
	
	mov 	r2, #8			@ numero de intervalos de tempo a esperar
	sub	r2, r2, r8		@ baseado no nivel

apaga_led:
	ldr	r1,=flag_timer
	ldr	r0,[r1]
	cmp	r0,#0			@ espera interrupcao do timer
	beq	apaga_led
	mov	r0,#0			@ reseta flag_timer
	str	r0,[r1]
	sub	r2, r2, #1
	cmp	r2, #0
	bne	apaga_led		@ avanca se tiver passado os N intervalos

	mov 	r9,#0
	strb 	r9,[r6]
	cmp 	r4, r8
	beq 	pre_verificacao		@ avanca para verificacao se acabaram os leds nesse nivel
	add 	r4, r4,#1

	mov 	r2, #8			@ numero de intervalos de tempo a esperar
	sub	r2, r2, r8		@ baseado no nivel

	b 	acende_led		@ avanca para proximo led da sequencia desse nivel
pre_verificacao:
	mov 	r4, #0			@ contador de botoes apertados
	mov 	r0, #-1
	ldr 	r1, =flag_botao		@ reseta flag_botao
	str 	r0, [r1]
verificacao:
	ldr 	r0, [r1]
	cmp 	r0, #-1
	beq 	verificacao		@ espera botao ser apertado
	cmp 	r0, #START
	beq	start_n			@ se for o start, comeca nova sequencia

	mov 	r2, r0
	mov 	r0, #-1     		@ reseta flag_botao
	str 	r0, [r1]
	ldr 	r0, =seq_atual
	ldr 	r0, [r0]
	ldrb 	r0, [r0,r4]		@ carregado valor do proximo led da sequencia
	cmp 	r2, r0
	bne 	fim_de_jogo		@ se estiver errado, acaba o jogo
	cmp 	r4, r8
	add 	r4, r4, #1
	bne 	verificacao		@ se acertou, mas nao acabou o jogo, avanca
	add 	r8, r8, #1
	cmp	r8, #8
	beq	fim_de_jogo		@ se acabou sequencia, acaba o jogo
	b 	pre_acende_led		@ se nao, avanca para proximo nivel


leds:
	.byte	0x8, 0x4, 0x2, 0x1	@ YELLOW, GREEN, BLUE, RED

seq_atual:				@endereco da seq atual
	.word 0

seq1:
	.byte RED, BLUE, BLUE, RED, GREEN, YELLOW, YELLOW, RED
seq2:
	.byte BLUE, BLUE, BLUE, RED, GREEN, GREEN, YELLOW, RED
seq3:
	.byte YELLOW, BLUE, BLUE, YELLOW, GREEN, YELLOW, YELLOW, RED
seq4:
	.byte RED, BLUE, BLUE, RED, GREEN, YELLOW, YELLOW, RED
seq5:
	.byte BLUE, YELLOW, BLUE, RED, GREEN, GREEN, YELLOW, GREEN
seq6:
	.byte YELLOW, BLUE, BLUE, YELLOW, GREEN, YELLOW, YELLOW, YELLOW
seq7:
	.byte YELLOW, BLUE, BLUE, RED, GREEN, YELLOW, YELLOW, RED
seq8:
	.byte BLUE, RED, BLUE, RED, GREEN, YELLOW, YELLOW, RED
seq9:
	.byte GREEN, BLUE, BLUE, YELLOW, GREEN, YELLOW, YELLOW, RED
seq10:
	.byte RED, BLUE, BLUE, RED, GREEN, RED, YELLOW, RED
seq11:
	.byte GREEN, RED, BLUE, RED, GREEN, YELLOW, YELLOW, RED
seq12:
	.byte YELLOW, BLUE, BLUE, YELLOW, GREEN, YELLOW, YELLOW, RED
seq13:
	.byte GREEN, BLUE, BLUE, RED, GREEN, GREEN, YELLOW, RED
seq14:
	.byte YELLOW, RED, BLUE, RED, GREEN, YELLOW, YELLOW, RED
seq15:
	.byte YELLOW, BLUE, RED, YELLOW, GREEN, YELLOW, YELLOW, YELLOW
seq16:
	.byte RED, BLUE, BLUE, RED, RED, YELLOW, BLUE, RED
seq17:
	.byte BLUE, RED, BLUE, RED, GREEN, RED, YELLOW, YELLOW
seq18:
	.byte GREEN, RED, RED, YELLOW, GREEN, BLUE, YELLOW, RED
seq19:
	.byte RED, BLUE, BLUE, RED, GREEN, YELLOW, YELLOW, RED
seq20:
	.byte BLUE, BLUE, BLUE, RED, BLUE, YELLOW, YELLOW, GREEN
seq21:
	.byte GREEN, BLUE, RED, YELLOW, GREEN, YELLOW, YELLOW, GREEN
seq22:
	.byte RED, BLUE, RED, RED, GREEN, BLUE, YELLOW, RED
seq23:
	.byte BLUE, RED, BLUE, RED, GREEN, YELLOW, GREEN, YELLOW
seq24:
	.byte GREEN, BLUE, BLUE, BLUE, GREEN, BLUE, YELLOW, RED
seq25:
	.byte RED, BLUE, BLUE, BLUE, GREEN, YELLOW, YELLOW, RED
seq26:
	.byte BLUE, BLUE, BLUE, RED, GREEN, YELLOW, YELLOW, RED
seq27:
	.byte GREEN, BLUE, BLUE, YELLOW, GREEN, GREEN, GREEN, RED
seq28:
	.byte RED, BLUE, BLUE, RED, GREEN, GREEN, YELLOW, RED
seq29:
	.byte BLUE, RED, BLUE, RED, GREEN, YELLOW, YELLOW, RED
seq30:
	.byte YELLOW, BLUE, RED, YELLOW, YELLOW, YELLOW, YELLOW, YELLOW

flag_timer:
	.word 0

flag_botao:
	.word -1

flag_check:
	.word 0

recorde:
	.byte 0

msg_game_over:
    .ascii      "GAME OVER!\n"
len_go = . - msg_game_over
nivel_alcancado:
    .ascii      "Nivel alcancado: "
nivel_alcancado_valor:
    .byte 0x30
    .ascii 	"\n"
len_nivel = . - nivel_alcancado
recorde_atual:
    .ascii      "Recorde atual: "
recorde_atual_valor:
    .byte 0x30
    .ascii 	"\n"
len_recorde_atual = . - recorde_atual
novo_recorde:
    .ascii      "Novo recorde: "
novo_recorde_valor:
    .byte 0x30
    .ascii 	"\n"
len_recorde = . - novo_recorde



@ tratador da interrupcao	
@ aqui quando timer expirou
	.align 4
tratador_timer:
	ldr	r9,=flag_timer		@ apenas liga a flag_timer
	mov	r10,#1
	str	r10,[r9]
	movs	pc,lr			@ e retorna

tratador_botao:
	mov 	r11,#-1              	@ guarda 0 em r9 caso não nenhum botão seja apertado

	ldr 	r9,=B_RED
	ldr 	r10,[r9]
	cmp 	r10,#B_READY         	@ testa se botão foi apertado
	moveq 	r11,#RED           	@ se sim, guarda o valor correspondente do botão em r9
	beq 	fim_tratador_botao  	@ e retorna
    
	ldr 	r9,=B_BLUE
	ldr 	r10,[r9]
	cmp 	r10,#B_READY
	moveq 	r11,#BLUE
	beq 	fim_tratador_botao
    
	ldr 	r9,=B_GREEN
	ldr 	r10,[r9]
	cmp 	r10,#B_READY
	moveq 	r11,#GREEN
	beq 	fim_tratador_botao
    
	ldr 	r9,=B_YELLOW
	ldr 	r10,[r9]
	cmp 	r10,#B_READY
	moveq 	r11,#YELLOW
	beq 	fim_tratador_botao
    
	ldr	r9,=B_START
	ldr	r10,[r9]
	cmp	r10,#B_READY
	moveq 	r11,#START
	addeq	r5,r5,#1
	beq 	fim_tratador_botao
    
fim_tratador_botao:
	ldr	r9,=flag_botao
	str 	r11,[r9]		@ guarda flag_botao
	movs	pc,lr			@ e retorna

fim_de_jogo:
	mov     r0, #1     		@ fd -> stdout
	ldr     r1, =msg_game_over   	@ buf -> msg
	ldr     r2, =len_go   		@ count -> len(msg)
	mov     r7, #4     		@ write is syscall #4
	svc     #0x5555    		@ invoke syscall
	ldr 	r0, =recorde
	ldrb	r1, [r0]
	cmp	r8, r1
	ble	mostra_recorde_atual
	
mostra_novo_recorde:
	strb	r8, [r0]
	ldr     r3, =recorde_atual_valor
	add	r6, r8, #0x30
	strb    r6, [r3]
	ldr     r3, =novo_recorde_valor
	add	r6, r8, #0x30
	strb    r6, [r3]
	mov     r0, #1     		@ fd -> stdout
	ldr     r1, =novo_recorde   	@ buf -> msg
	ldr     r2, =len_recorde   	@ count -> len(msg)
	mov     r7, #4     		@ write is syscall #4
	svc     #0x5555    		@ invoke syscall
	b start

mostra_recorde_atual:
	ldr 	r1, =nivel_alcancado_valor
	add	r6, r8, #0x30
	strb    r6, [r1]
	mov     r0, #1     		@ fd -> stdout
	ldr     r1, =nivel_alcancado   	@ buf -> msg
	ldr     r2, =len_nivel   	@ count -> len(msg)
	mov     r7, #4     		@ write is syscall #4
	svc     #0x5555    		@ invoke syscall
	mov     r0, #1     		@ fd -> stdout
	ldr     r1, =recorde_atual   	@ buf -> msg
	ldr     r2, =len_recorde_atual  @ count -> len(msg)
	mov     r7, #4     		@ write is syscall #4
	svc     #0x5555    		@ invoke syscall
	b start

fim:
