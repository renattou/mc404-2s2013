@ MC404 - LAb09
@ VITOr ALVES ArrAIS DE SOUZA 120263
@ rENATO VArgAs


@ modos de interrupção no registrador de status
	.set IRQ_MODE,	0x12
	.set USER_MODE,	0x10

@ flag para habilitar interrupções externas no registrador de status
	.set IRQ, 0x80

@enderecos dispositivos
	.set LEDS,	0x90110
	.set TIMER,	0x90008
	.set STARTBUTT, 0x80011
	.set REDBUTT, 	0x80012
	.set BLUEBUTT, 	0x80013
	.set GREENBUTT, 0x80014
	.set YELLOWBUTT,0x80015

@ constantes
	.set INTERVAL,1000
	.set RED, 	0
	.set BLUE, 	1
	.set GREEN, 	2
	.set YELLOW, 	3

@ vetor de interrupções
	.org	0x0
vetor_int:
	ldr	pc, =trata_timer
	ldr	pc, =trata_startbutton
	ldr	pc, =trata_colorbutton

	.org 0x1000
main:
	mov	sp,#0x400	@ seta pilha do modo supervisor
	mov	r0,#IRQ_MODE	@ coloca processador no modo IRQ (interrupção externa)
	msr	cpsr,r0		@ processador agora no modo IRQ
	mov	sp,#0x300	@ seta pilha de interrupção IRQ
	mov	r0,#USER_MODE	@ coloca processador no modo usuário
	bic     r0,r0,#IRQ      @ interrupções IRQ habilitadas
	msr	cpsr,r0		@ processador agora no modo usuário
	mov	sp,#0x10000	@ pilha do usuário no final da memória 

	ldr	r0,=INTERVAL
	ldr	r6,=TIMER
	str  	r0,[r6]		@ seta timer
loop:
	@ verifica interrupcoes
	@ start button
	@ color button
	@ time
	@ bne loop
	
	@ realiza algo
	@b	loop


flag_start:			@ flag para saber se 
	.word 0			@ start button foi pressionado

flag_color:			@ flag para saber 
	.word 0			@ qual cor foi pressionada

@ tratadores de interrupcao	
	.align 4
trata_timer:
	movs	pc,lr		@ retorna
trata_startbutton:
	movs	pc,lr		@ retorna
trata_colorbutton:
	movs	pc,lr		@ retorna


@@@@@@@@@@@@@ ESSA PARTE VAI MUDAR @@@@@@@@@@@@
seq1:
    .word	'g','b','r','y','r','g','b','g','r','y','b','g','b','b','y','r','y','r','r','g'

seq2:
    .word	'r','y','b','b','g','r','y','y','r','g','b','y','b','r','g','y','b','g','g','b'

seq3:
    .word	'r','r','y','g','b','b','g','r','y','y','y','b','r','g','g','y','r','r','b','g'

seq4:
    .word	'b','r','g','y','g','g','r','y','b','g','r','r','g','b','g','r','y','g','b','r'

seq5:
    .word	'g','g','b','g','y','r','g','r','y','b','g','y','y','y','y','g','b','r','y','g'

seq6:
    .word	'y','r','g','b','g','g','b','b','r','g','y','g','b','r','b','r','g','g','r','y'

seq7:
    .word	'y','b','y','r','g','g','b','r','g','b','y','g','b','r','y','g','b','r','g','g'

seq8:
    .word	'g','r','g','r','g','b','y','g','b','y','g','r','g','b','y','g','b','r','y','g'

seq9:
    .word	'r','r','r','g','y','g','b','r','y','g','b','y','g','r','y','g','b','y','r','g'

seq10:
    .word	'y','g','r','g','b','g','r','r','g','b','y','y','y','g','b','r','y','g','b','b'

seq11:
    .word	'g','y','y','g','b','y','y','y','g','b','r','y','g','b','r','y','g','b','r','y'

seq12:
    .word	'g','r','g','g','g','r','r','y','b','y','b','b','b','y','b','y','b','r','b','y'

seq13:
    .word	'b','b','b','y','r','b','b','g','y','g','r','y','b','r','y','b','r','y','b','g'

seq14:
    .word	'g','y','b','g','r','g','y','y','y','g','b','b','g','r','g','r','g','b','y','b'

seq15:
    .word	'g','r','r','r','b','g','y','g','y','b','g','g','b','g','r','r','r','g','b','y'

seq16:
    .word	'y','y','g','y','g','y','b','r','b','y','b','r','y','b','r','y','b','r','y','b'

seq17:
    .word	'r','g','b','y','r','g','b','y','r','g','b','y','r','g','b','y','r','g','b','y'

seq18:
    .word	'y','g','y','g','y','b','g','y','b','g','r','g','y','b','g','y','b','r','b','y'

seq19:
    .word	'y','y','b','g','y','g','r','r','g','b','y','g','r','y','b','g','y','b','y','g'

seq20:
    .word	'g','y','b','y','g','g','g','y','b','g','r','r','y','g','r','y','b','g','r','r'

seq21:
    .word	'g','b','y','b','g','r','g','y','b','y','b','y','g','b','r','g','g','r','b','b'

seq22:
    .word	'g','y','b','g','y','y','y','g','b','b','g','y','r','y','y','b','b','y','b','y'

seq23:
    .word	'y','g','y','g','b','g','g','y','r','r','y','g','b','r','b','y','g','r','b','y'

seq24:
    .word	'y','g','b','r','g','r','r','b','y','g','g','r','g','y','b','y','g','r','g','b'

seq25:
    .word	'r','g','r','r','b','g','b','y','y','b','y','r','b','g','y','b','r','g','y','r'

seq26:
    .word	'g','r','b','y','y','b','g','g','g','y','b','r','y','b','g','y','b','r','r','r'

seq27:
    .word	'g','g','g','r','r','r','g','r','b','r','g','y','y','y','g','b','y','g','r','g'

seq28:
    .word   'g','y','r','y','r','r','g','r','g','r','g','y','y','y','g','y','g','y','g','y'

seq29:
    .word	'b','b','b','g','g','r','g','b','y','y','g','b','r','g','b','y','g','b','b','g'

seq30:
    .word	'y','y','g','b','g','g','g','b','r','y','b','g','r','b','g','y','g','b','r','g'
