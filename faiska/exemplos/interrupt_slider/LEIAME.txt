Este exemplo mostra como utilizar um botão com interrupções.

Note que no caso a interrupção não traz muita vantagem, já
que o processador não tem outra tarefa a fazer!

Para executar, compile

fasm test_interrupt_button.fsk -o int_button.exe

e inicie o simulador

jfm -l int_button.exe -d device_button_interrupt.txt

No simulador, execute "g main".
